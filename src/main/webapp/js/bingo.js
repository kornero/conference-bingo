"use strict";

$(function () {
    var TABLE_SIZE = 5;

    var bingoFlag = false;
    var table = $('#bingo-table');
    var buttons = table.find('.btn-default');

    function bingo(elements) {
        elements.addClass('btn-success');

        if (bingoFlag) {
            return;
        }

        // buttons.attr('disabled', 'disabled');
        alert('BINGO!');

        bingoFlag = true;
    }

    $.getJSON("js/words.json", function (words) {
        buttons.each(function () {
            var index = Math.floor(words.length * Math.random());
            var word = words[index];

            var button = $(this);
            button.text(word.text);
            button.attr('data-index', index);
        })
    });

    buttons.click(function () {

        if (bingoFlag) {
            return;
        }

        // Toggle cell.
        var index = $(this).attr('data-index');
        buttons.filter('[data-index="' + index + '"]').toggleClass('btn-primary');

        // Check rows.
        table.children('.row').each(function () {
            var elements = $(this).find('.btn-primary');
            if (elements.length == TABLE_SIZE) {
                bingo(elements);
            }
        });

        // Check columns.
        for (var i = 0; i < TABLE_SIZE; i++) {
            var elements = table.children('.row').find('.btn:eq(' + i + ')').filter('.btn-primary');
            if (elements.length == TABLE_SIZE) {
                bingo(elements);
            }
        }
    })
});